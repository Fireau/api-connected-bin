'use strict';

const config = require('config');
const uuid = require('uuid/v4');
const Container = require('./Container');

const db = require('../services/db').skeleton();

module.exports = class Bins {
  constructor({
    buid,
    uid,
    interaction_number,
  }) {
    this.uid = buid ? buid : uid;
    this.interaction_number = interaction_number || 0;
  }

  static tableName() {
    return `${config.get('connected_bin.schema')}.bins`;
  }

  static list($list) {
    const query = `
    SELECT bin.uid as buid,
    bin.interaction_number,
    cont.*
    FROM ${Bins.tableName()} as bin
    JOIN ${Container.tableName()} as cont on bin.uid = cont.bin_uid`;
      
    db.query(query, (err, res) => {
      if (err) {
        return $list(err);
      }

      const bins = [];
      const cont = [];
      for (let i = 0; i < res.rows.length; i++) {
        cont.push(new Container(res.rows[i]))
      }
      let bin = new Bins(res.rows[0])
      bin['container'] = cont
      bins.push(bin);
      $list(null, bins || []);
    });
  }

  static get(uid, $get) {
    const query = `SELECT * 
      FROM ${Bins.tableName()}
      WHERE uid = $1;`;
    const args = [uid];

    db.query(query, args, (err, res) => {
      if (err) {
        return $get(err);
      }

      $get(null, res.rowCount === 0 ? null : new Bins(res.rows[0]));
    });
  }

  addOneWasteNumber($addOneWasteNumber) {
    const query = `UPDATE ${Bins.tableName()}
      SET waste_numbers = waste_numbers + 1
      WHERE uid = $1
      RETURNING * ;`;

    const args = [this.uid];

    db.query(query, args, (err, res) => {
      if (err) {
        return $addOneWasteNumber(err);
      }

      $addOneWasteNumber(null, res.rowCount === 0 ? null : new Bins(res.rows[0]));
    });
  }

  addOneInteraction($addOneInteraction) {
    const query = `UPDATE ${Bins.tableName()}
      SET interaction_number = interaction_number + 1
      WHERE uid = $1
      RETURNING *;
      `;
    const args = [this.uid];

    db.query(query, args, (err, res) => {
      if (err) {
        return $addOneInteraction(err);
      }

      $addOneInteraction(null, res.rowCount === 0 ? null : new Bins(res.rows[0]));
    });
  }

  insert($insert) {
    this.uid = uuid();
    const query = `INSERT INTO ${Bins.tableName()}(uid, type_dechet, waste_numbers, interaction_number,total_capacity)
      VALUES ($1, $2, $3, $4, $5)`;
    const args = [this.uid, this.type_dechet, this.waste_numbers, this.interaction_number, this.total_capacity];

    db.query(query, args, (err, res) => {
      if (err || res.rowCount === 0) {
        return $insert(err);
      }

      $insert(null, this);
    });
  }

  update($update) {
    const query = `UPDATE ${Bins.tableName()} 
      SET type_dechet = $1,
      waste_numbers = $2,
      interaction_number = $3,
      total_capacity = $4
      WHERE uid = $5`;
    const args = [this.type_dechet, this.waste_numbers, this.interaction_number, this.total_capacity, this.uid];

    db.query(query, args, (err, res) => {
      if (err) {
        return $update(err);
      }

      $update(null, res.rowCount === 0 ? null : this);
    });
  }

  static delete(uid, $delete) {
    const query = `DELETE FROM ${Bins.tableName()}
      WHERE uid = $1`;
    const args = [uid];
    db.query(query, args, (err, res) => {
      if (err) {
        return $delete(err);
      }

      $delete(null, res.rowCount === 0 ? null : {
        success: true
      });
    });
  }
};