'use strict';

const config = require('config');
const uuid = require('uuid/v4');

const db = require('../services/db').skeleton();

module.exports = class Container {
  constructor({
    uid,
    bin_uid,
    type_dechet,
    waste_numbers,
    total_capacity
  }) {
    this.uid = uid;
    this.bin_uid = bin_uid  || 0;
    this.type_dechet = type_dechet || '';
    this.waste_numbers = waste_numbers || 0;
    this.total_capacity = total_capacity || 500;
  }

  static tableName() {
    return `${config.get('connected_bin.schema')}.container`;
  }

  static list($list) {
    const query = `SELECT * 
      FROM ${Container.tableName()};`;

    db.query(query, (err, res) => {
      if (err) {
        return $list(err);
      }

      const users = [];
      for (let i = 0; i < res.rows.length; i++) {
        users.push(new Container(res.rows[i]));
      }

      $list(null, users || []);
    });
  }

  static get(uid, $get) {
    const query = `SELECT * 
      FROM ${Container.tableName()}
      WHERE uid = $1;`;
    const args = [uid];

    db.query(query, args, (err, res) => {
      if (err) {
        return $get(err);
      }

      $get(null, res.rowCount === 0 ? null : new Container(res.rows[0]));
    });
  }

  addOneWasteNumber($addOneWasteNumber) {
    const query = `UPDATE ${Container.tableName()}
      SET waste_numbers = waste_numbers + 1
      WHERE uid = $1
      RETURNING * ;`;

    const args = [this.uid];

    db.query(query, args, (err, res) => {
      if (err) {
        return $addOneWasteNumber(err);
      }

      $addOneWasteNumber(null, res.rowCount === 0 ? null : new Container(res.rows[0]));
    });
  }

  insert($insert) {
    this.uid = uuid();
    const query = `INSERT INTO ${Container.tableName()}(uid, bin_uid, type_dechet, waste_numbers,total_capacity)
      VALUES ($1, $2, $3, $4, $5)`;
    const args = [this.uid, this.bin_uid, this.type_dechet, this.waste_numbers, this.total_capacity];

    db.query(query, args, (err, res) => {
      if (err || res.rowCount === 0) {
        return $insert(err);
      }

      $insert(null, this);
    });
  }

  update($update) {
    const query = `UPDATE ${Container.tableName()} 
      SET type_dechet = $1,
      waste_numbers = $2,
      bin_uid = $3,
      total_capacity = $4
      WHERE uid = $5`;
    const args = [this.type_dechet, this.waste_numbers, this.bin_uid, this.total_capacity, this.uid];

    db.query(query, args, (err, res) => {
      if (err) {
        return $update(err);
      }

      $update(null, res.rowCount === 0 ? null : this);
    });
  }

  static delete(uid, $delete) {
    const query = `DELETE FROM ${Container.tableName()}
      WHERE uid = $1`;
    const args = [uid];
    db.query(query, args, (err, res) => {
      if (err) {
        return $delete(err);
      }

      $delete(null, res.rowCount === 0 ? null : {
        success: true
      });
    });
  }
};