'use strict';

const Ctrl = require('./Ctrl');
const Bins = require('../models/Bins');
const errors = require('restify-errors');

module.exports = class BinsCtrl extends Ctrl {
  constructor() {
    super({
      Model: Bins,
      modelLabel: 'bins'
    });
  }

  validate(req) {
    const requiredErrors = [];

    const required = ['type_dechet'];

    for (let i = 0; i < required.length; i++) {
      const field = required[i];
      if (!req.body[field]) {
        requiredErrors.push(field);
      }
    }

    return requiredErrors.length > 0 ? new Error(`${requiredErrors.join(',')} are required and can't be empty`) : null;
  }

  addOneInteraction(req, $addOneInteraction) {
    if (!req.params.uid) {
      console.log('Params uid is missing');
      return $addOneInteraction(new errors.BadRequestError('Missing required parameters'));
    }

    const modelInst = this.getModelInstance(req);
    modelInst.addOneInteraction((err, res) => {
      if (err) {
        console.log(`Failed to fetch ${this.modelLabel}(${req.params.uid})`);
        return $addOneInteraction(new errors.InternalServerError(err, 'Database error'));
      }

      if (!res) {
        return $addOneInteraction(new errors.ResourceNotFoundError(`${this.modelLabel}(${req.params.uid}) does not exist`));
      }

      $addOneInteraction(null, res);
    });
  }

  getModelInstance(req) {
    const data = {
      interaction_number: req.body.interaction_number,
    };

    // If we the route contains the uid add it
    if (req.params.uid) {
      data.uid = req.params.uid;
    }

    return new this.Model(data);
  }
};