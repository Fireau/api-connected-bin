'use strict';

const errors = require('restify-errors');

module.exports = class Ctrl {
  constructor({
    Model,
    modelLabel
  }) {
    this.Model = Model;
    this.modelLabel = modelLabel;
  }

  list(req, $list) {
    this.Model.list((err, res) => {
      if (err) {
        console.log(`Failed to fetch the list of ${this.modelLabel}`);
        return $list(new errors.InternalServerError(err, 'Database error'));
      }

      $list(null, {
        bins: res
      });
    });
  }

  get(req, $get) {
    if (!req.params.uid) {
      console.log('Params uid is missing');
      return $get(new errors.BadRequestError('Missing required parameters'));
    }

    this.Model.get(req.params.uid, (err, res) => {
      if (err) {
        console.log(`Failed to fetch ${this.modelLabel}(${req.params.uid})`);
        return $get(new errors.InternalServerError(err, 'Database error'));
      }

      if (!res) {
        return $get(new errors.ResourceNotFoundError(`${this.modelLabel}(${req.params.uid}) does not exist`));
      }

      $get(null, res);
    });
  }

  insert(req, $insert) {
    const validationError = this.validate(req);
    if (validationError) {
      console.log('Missing required params');
      return $insert(new errors.BadRequestError(validationError, 'Missing required parameters'));
    }

    const modelInst = this.getModelInstance(req);
    modelInst.insert((err, res) => {
      if (err) {
        console.log(`Failed to insert ${this.modelLabel}`);
        return $insert(new errors.InternalServerError(err, 'Database error'));
      }

      $insert(null, res);
    });
  }

  update(req, $update) {
    if (!('uid' in req.params)) {
      console.log('Missing required params');
      return $update(new errors.BadRequestError('Missing required parameters : uid'));
    }

    const modelInst = this.getModelInstance(req);
    modelInst.update((err, res) => {
      if (err) {
        console.log(`Failed to update ${this.modelLabel}`);
        return $update(new errors.InternalServerError(err, 'Database error'));
      }

      if (!res) {
        return $update(new errors.ResourceNotFoundError(`${this.modelLabel}(${req.params.uid}) does not exist`));
      }

      $update(null, res);
    });
  }

  delete(req, $delete) {
    if (!req.params.uid) {
      console.log('Params uid is missing');
      return $delete(new errors.BadRequestError('Missing required parameters'));
    }

    this.Model.delete(req.params.uid, (err, res) => {
      if (err) {
        console.log('Failed to delete data');
        return $delete(new errors.InternalServerError(err, 'Database error'));
      }

      if (!res) {
        return $delete(new errors.ResourceNotFoundError(`${this.modelLabel}(${req.params.uid}) does not exist`));
      }

      $delete(null, res);
    });
  }

  // Returns an instance of Error containing the detailed information if something is wrong, null otherwise
  validate(req) {}

  // Returns an isntance of this.Model
  getModelInstance(req) {}
};