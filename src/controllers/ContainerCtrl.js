'use strict';

const Ctrl = require('./Ctrl');
const Container = require('../models/Container');
const errors = require('restify-errors');

module.exports = class ContainerCtrl extends Ctrl {
  constructor() {
    super({
      Model: Container,
      modelLabel: 'container'
    });
  }

  validate(req) {
    const requiredErrors = [];

    const required = ['type_dechet'];

    for (let i = 0; i < required.length; i++) {
      const field = required[i];
      if (!req.body[field]) {
        requiredErrors.push(field);
      }
    }

    return requiredErrors.length > 0 ? new Error(`${requiredErrors.join(',')} are required and can't be empty`) : null;
  }


  getEmptyPercent(req, $getEmptyPercent) {
    if (!req.params.uid) {
      console.log('Params uid is missing');
      return $getEmptyPercent(new errors.BadRequestError('Missing required parameters'));
    }
    this.Model.get(req.params.uid,(err, res) => {
      if (err) {
        console.log(`Failed to fetch ${this.modelLabel}(${req.params.uid})`);
        return $getEmptyPercent(new errors.InternalServerError(err, 'Database error'));
      }

      if (!res) {
        return $getEmptyPercent(new errors.ResourceNotFoundError(`${this.modelLabel}(${req.params.uid}) does not exist`));
      }
      res.emptyPercent = (res.waste_numbers / res.total_capacity)* 100
      $getEmptyPercent(null, res);
    });
  }

  addOneWasteNumber(req, $addOneWasteNumber) {
    if (!req.params.uid) {
      console.log('Params uid is missing');
      return $addOneWasteNumber(new errors.BadRequestError('Missing required parameters'));
    }
    const modelInst = this.getModelInstance(req);
    modelInst.addOneWasteNumber((err, res) => {
      if (err) {
        console.log(`Failed to fetch ${this.modelLabel}(${req.params.uid})`);
        return $addOneWasteNumber(new errors.InternalServerError(err, 'Database error'));
      }

      if (!res) {
        return $addOneWasteNumber(new errors.ResourceNotFoundError(`${this.modelLabel}(${req.params.uid}) does not exist`));
      }

      $addOneWasteNumber(null, res);
    });
  }

  getModelInstance(req) {
    const data = {
      type_dechet: req.body.type_dechet,
      waste_numbers: req.body.waste_numbers,
      total_capacity: req.body.total_capacity
    };

    // If we the route contains the uid add it
    if (req.params.uid) {
      data.uid = req.params.uid;
    }

    return new this.Model(data);
  }
};