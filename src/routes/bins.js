'use strict';

const BinsCtrl = require('../controllers/BinsCtrl');

const binsCtrl = new BinsCtrl();

module.exports = (server) => {
  server.get('/bins', (req, res, next) => {
    binsCtrl.list(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });
  server.get('/bins/:uid', (req, res, next) => {
    binsCtrl.get(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });
  server.post('/bins', (req, res, next) => {
    binsCtrl.insert(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });

  server.post('/bins/addOneInteraction/:uid', (req, res, next) => {
    binsCtrl.addOneInteraction(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });

  server.put('/bins/:uid', (req, res, next) => {
    binsCtrl.update(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });
  server.del('/bins/:uid', (req, res, next) => {
    binsCtrl.delete(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });
};