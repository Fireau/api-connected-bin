'use strict';

const ContainerCtrl = require('../controllers/ContainerCtrl');

const containerCtrl = new ContainerCtrl();

module.exports = (server) => {
  server.get('/container', (req, res, next) => {
    containerCtrl.list(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });

  server.get('/container/:uid', (req, res, next) => {
    containerCtrl.get(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });

  server.get('/container/emptyPercent/:uid', (req, res, next) => {
    containerCtrl.getEmptyPercent(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });

  server.post('/container', (req, res, next) => {
    containerCtrl.insert(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });

  server.post('/container/addOneWasteNumber/:uid', (req, res, next) => {
    containerCtrl.addOneWasteNumber(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });

  server.put('/container/:uid', (req, res, next) => {
    containerCtrl.update(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });
  server.del('/container/:uid', (req, res, next) => {
    containerCtrl.delete(req, (err, data) => {
      res.send(err || data);
      next();
    });
  });
};