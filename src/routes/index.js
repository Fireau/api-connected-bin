'use strict';

module.exports = (server) => {
  require('./bins')(server);
  require('./container')(server);
};