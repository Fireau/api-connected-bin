'use strict';

const pg = require('pg');
const config = require('config');

const dbs = new Map();

class Db {
  constructor(dsn) {
    const conf = {
      connectionString: dsn,
      max: 10,
      idleTimeoutMillis: 10000,
    };
    this.pool = new pg.Pool(conf);
    this.dsn = dsn;
  }

  /**
   * Execute a query and return its result
   */
  query(query, args, $query) {
    if (!$query && args instanceof Function) {
      $query = args;
      args = null;
    }

    this.pool.connect((err, client, $connect) => {
      if (err) {
        return console.info(err, `error fetching client for ${this.dsn} from pool`);
      }

      client.query(query, args, (err, res) => {
        // call `$connect(err)` to release the client back to the pool (or destroy it if there is an error)
        $connect(err);

        if (err) {
          $query(err, null);
          return;
        }

        $query(null, res);
      });
    });
  }
}

module.exports = {
  // In case your application needs to connect to several databases
  // add other databases here similarly to connected_bin
  skeleton() {
    if (dbs.has('skeleton')) {
      return dbs.get('skeleton');
    }

    const db = new Db(config.get('connected_bin.dsn'));
    dbs.set('skeleton', db);

    return db;
  }
};