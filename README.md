# Connected-bin-api

skeleton-api is a project intending to provide a basis to communicate between IOT sensors from a bin to an react native mobile application.

## Requirements

* `node` (version >= 13.8.*)
* `postgreSQL`

Only if you wish to use the docker environment
* `docker`
* `docker-compose`


## Configurations

If the `NODE_ENV` environment variable is not set the default configuration will be used
To switch between configuration set the `NODE_ENV` environment variable to one of these values :
* `dev`
* `prex`
* `prod`

`dev` configuration is designed to be used with the docker environment described by the files `Dockerfile` and `docker-compose.yml`.
In order to use this environment, you'll have to install `docker` and `docker-compose`, then simply run `docker-compose up --build`

You can also override the configurations defined in the config files by setting the encironment variables as defined in `custom-environment-variables.json`.

## Run the app

Using docker
* `npm install`
* `docker-compose up --build`

## Test the app

Insomnia or Postman can be used to test the API easily. 
An insomnia export containing all the routes is available under `/doc`


## Documentation

# Bins

## Endpoint: `/bins`

### GET

Fetch all the bins.

Example: `/bins`

**Response**
```json
{
  "bins": [
    {
      "uid": "b2f8edd9-6065-43d0-aaf0-b1e4a74c7971",
      "type_dechet": "Bio",
      "waste_numbers": "0",
      "interaction_number": "0",
      "total_capacity": "500"
    },
    {
      "uid": "52a0e630-3de8-40c7-b9c7-7fbfde34dd9a",
      "type_dechet": "Carton",
      "waste_numbers": "0",
      "interaction_number": "0",
      "total_capacity": "500"
    },
    {
      "uid": "8da2ceee-1581-4267-a6f4-bcd52d137d90",
      "type_dechet": "Plastique",
      "waste_numbers": "0",
      "interaction_number": "0",
      "total_capacity": "500"
    }
  ]
}
```

### POST

Create a user.

* Parameters (all parameters are required):
  * Body:
    * `type_dechet` - enum "Bio" / "Carton" / "Plastique"
    * `waste_numbers` - int
    * `interaction_number` - int
    * `total_capacity` - int

Example: `/bins`

**Body**
```json
{
	"type_dechet": "Bio",
	"waste_numbers" : 0,
	"interaction_number": 0,
	"total_capacity": 200
}
```

**Response**
```json
{
	"uid": "b2f8edd9-6065-43d0-aaf0-b1e4a74c7971",
	"type_dechet": "Bio",
	"waste_numbers" : 0,
	"interaction_number": 0,
	"total_capacity": 200
}
```


## Endpoint: `/bins/:uid`

### GET

Retrieve a user from its uid.

* Parameters:
  * Url:
    *  `uid` - UUIDv4

Example: `/bins/b2f8edd9-6065-43d0-aaf0-b1e4a74c7971`

**Reponse**
```json
{
  "uid": "b2f8edd9-6065-43d0-aaf0-b1e4a74c7971",
  "type_dechet": "Bio",
  "waste_numbers": "0",
  "interaction_number": "0",
  "total_capacity": "500"
}
```

### PUT

Update a bin.

* Parameters:
  * Url:
    *  `uid` - UUIDv4
  * Body:
    * `type_dechet` - enum "Bio" / "Carton" / "Plastique"
    * `waste_numbers` - int
    * `interaction_number` - int
    * `total_capacity` - int
    
Example: `/bins/b2f8edd9-6065-43d0-aaf0-b1e4a74c7971`

**Body**
```json
{
	"waste_number":1
}
```

**Response**
```json
{
  "uid": "b2f8edd9-6065-43d0-aaf0-b1e4a74c7971",
  "type_dechet": "Bio",
  "waste_numbers": "1",
  "interaction_number": "0",
  "total_capacity": "500"
}
```

### DELETE

Delete a user.

* Parameters:
  * Url:
    * `uid` - UUIDv4

Example: `/bins/b2f8edd9-6065-43d0-aaf0-b1e4a74c7971`

**Response**
```json
{
	"success": true
}
```

# Errors

If an error happens during a request, the response will have a status code corresponding 
to the type of error and the response body will have at least the following information:

```json
{
	"code": "",
	"message": ""
}

If necessary we might add a field `details` containing more detailed information.

## 500

Raised when a servor error occurs.

```json
{
	"code": "InternalServer",
	"message": "Database error; caused by Error: ..."
}
```

## 400

Raised when the request parameters are invalid or incomplete.

```json
{
	"code": "BadRequest",
	"message": "Missing required parameters"
}
```

## 404

Raised when the route or the resource doesn't exist.

```json
{
	"code": "ResourceNotFound",
	"message": "bins(b2f8edd9-6065-43d0-aaf0-b1e4a74c7971 does not exist"
}
```