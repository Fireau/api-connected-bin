'use strict';

const config = require('config');
const restify = require('restify');

const routes = require('./src/routes');

const server = restify.createServer({
  name: 'api-skeleton'
});

server.use(restify.plugins.requestLogger());
server.use(restify.plugins.gzipResponse());

// make query string params, and body params available under req.query and req.body
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

// Register the routes
routes(server);

server.listen(config.get('server.port'), () => {
  console.log('Serveur runnings on', server.url);
});