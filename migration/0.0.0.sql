CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TYPE bin_dechet_type AS ENUM ('Bio', 'Carton', 'Papier/Plastique');

CREATE SCHEMA api;

CREATE TABLE api.bins (
  uid UUID PRIMARY KEY,
  interaction_number NUMERIC NOT NULL DEFAULT 0
);

CREATE TABLE api.container (
  uid UUID PRIMARY KEY,
  bin_uid UUID NOT NULL REFERENCES api.bins (uid),
  type_dechet bin_dechet_type NOT NULL,
  waste_numbers NUMERIC NOT NULL DEFAULT 0, 
  total_capacity NUMERIC NOT NULL DEFAULT 500
);

INSERT INTO api.bins (uid) VALUES 
('f8ec6f66-639f-11ea-bc55-0242ac130003');

INSERT INTO api.container (uid, bin_uid, type_dechet,waste_numbers) VALUES 
('b2f8edd9-6065-43d0-aaf0-b1e4a74c7971','f8ec6f66-639f-11ea-bc55-0242ac130003','Bio',0),
('52a0e630-3de8-40c7-b9c7-7fbfde34dd9a','f8ec6f66-639f-11ea-bc55-0242ac130003','Carton',100),
('8da2ceee-1581-4267-a6f4-bcd52d137d90','f8ec6f66-639f-11ea-bc55-0242ac130003','Papier/Plastique',400);