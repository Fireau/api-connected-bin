FROM node:13-alpine
ADD . /app
WORKDIR /app
ENV NODE_ENV dev
ENV LOGGER_CONSOLE_OUTPUT 1
CMD ["npm", "run", "watch"]

